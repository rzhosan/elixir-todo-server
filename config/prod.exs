use Mix.Config

config :logger,
  backends: [LoggerJSON]

config :logger_json, :backend,
  metadata: :all
