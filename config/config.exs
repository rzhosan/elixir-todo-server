use Mix.Config

config :todo, http_port: 5454

config :logger, :console,
  format: "\n$time $metadata[$level] $levelpad$message\n",
  metadata: [:pid, :mfa, :line],
  colors: [enabled: true, info: :cyan]

import_config "#{Mix.env()}.exs"
