defmodule Todo.Item do
  defstruct id: nil, date: nil, title: nil

  def new(date, title) do
    %Todo.Item{date: date, title: title}
  end
end
