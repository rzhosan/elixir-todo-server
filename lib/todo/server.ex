defmodule Todo.Server do
  use GenServer, restart: :temporary

  require Logger

  def start_link(todo_list_name, file_name \\ nil) do
    Logger.info("Starting to-do server for #{todo_list_name}", todo_list_name: todo_list_name, file_name: file_name)

    GenServer.start_link(__MODULE__, {todo_list_name, file_name}, name: via_tuple(todo_list_name))
  end

  defp via_tuple(name) do
    Todo.ProcessRegistry.via_tupple({__MODULE__, name})
  end

  def import_csv(todo_server, file_name) do
    File.stream!(file_name)
      |> Enum.map(fn entry ->
        [raw_date, title] = String.split(entry, ",")
        [year, month, day] = String.split(raw_date, "-")
          |> Enum.map(&String.to_integer/1)
        {:ok, date} = Date.new(year, month, day)

        Todo.Item.new(date, String.replace(title, "\n", ""))
      end)
      |> Enum.each(fn entry -> add_entry(todo_server, entry) end)
  end

  def add_entry(todo_server, new_entry) do
    GenServer.cast(todo_server, {:add_entry, new_entry})
  end

  def update_entry(todo_server, id, updater) do
    GenServer.cast(todo_server, {:update_entry, id, updater})
  end

  def delete_entry(todo_server, id) do
    GenServer.cast(todo_server, {:delete_entry, id})
  end

  def entries(todo_server, date) do
    GenServer.call(todo_server, {:entries, date})
  end

  @impl true
  def init({todo_list_name, nil}) do
    {:ok, Todo.Database.get(todo_list_name) || Todo.List.new(todo_list_name)}
  end

  @impl true
  def handle_cast({:add_entry, new_entry}, todo_list) do
    new_list = Todo.List.add_entry(todo_list, new_entry)
    Todo.Database.store(todo_list.name, new_list)

    {:noreply, new_list}
  end

  @impl true
  def handle_cast({:update_entry, id, updater}, todo_list) do
    new_list = Todo.List.update_entry(todo_list, id, updater)
    Todo.Database.store(todo_list.name, new_list)

    {:noreply, new_list}
  end

  @impl true
  def handle_cast({:delete_entry, id}, todo_list) do
    new_list = Todo.List.delete_entry(todo_list, id)
    Todo.Database.store(todo_list.name, new_list)

    {:noreply, new_list}
  end

  @impl true
  def handle_call({:entries, date}, _, todo_list) do
    {:reply, Todo.List.entries(todo_list, date), todo_list}
  end
end
