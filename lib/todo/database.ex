defmodule Todo.Database do
  require Logger

  @db_folder ".db"
  @workers_count 3

  def child_spec(_) do
    Logger.info("Starting database server.")

    if !File.exists?(@db_folder) do
      File.mkdir!(@db_folder)
    end

    :poolboy.child_spec(
      __MODULE__,
      [
        name: {:local, __MODULE__},
        worker_module: Todo.DatabaseWorker,
        size: @workers_count
      ],
      [@db_folder]
    )
  end

  def store(key, data) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid -> Todo.DatabaseWorker.store(worker_pid, key, data)
    end)
  end

  def get(key) do
    :poolboy.transaction(
      __MODULE__,
      fn worker_pid -> Todo.DatabaseWorker.get(worker_pid, key)
    end)
  end
end
