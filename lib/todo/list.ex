defmodule Todo.List do
  defstruct auto_id: 1, name: nil, entries: %{}

  def new(name \\ nil, entries \\ []) do
    Enum.reduce(
      entries,
      %Todo.List{name: name},
      &add_entry(&2, &1)
    )
  end

  def add_entry(%Todo.List{auto_id: auto_id, name: name, entries: entries}, %{date: date, title: title}) do
    entry = %Todo.Item{id: auto_id, date: date, title: title}
    new_entries = Map.put(entries, entry.id, entry)

    %Todo.List{auto_id: auto_id + 1, name: name, entries: new_entries}
  end

  def delete_entry(%Todo.List{auto_id: auto_id, name: name, entries: entries}, id) do
    %Todo.List{auto_id: auto_id, name: name, entries: Map.delete(entries, id)}
  end

  def update_entry(todo_list = %Todo.List{auto_id: auto_id, name: name, entries: entries}, id, updater) do
    case Map.fetch(entries, id) do
      {:ok, _} ->
        entries = Map.update!(entries, id, updater)
        %Todo.List{auto_id: auto_id, name: name, entries: entries}
      :error -> todo_list
    end
  end

  def entries(%Todo.List{entries: entries}, date) do
    Map.values(entries)
      |> Enum.filter(fn entry -> entry.date == date end)
  end
end
