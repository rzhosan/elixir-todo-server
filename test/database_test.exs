defmodule Todo.Database.Test do
  use ExUnit.Case
  doctest Todo.Database

  setup do
    remove_test_files()

    on_exit(fn -> remove_test_files() end)
  end

  test "stores data and reads it" do
    data = %{name: "Alice", info: %{id: 5, gender: "F", colors: [:blue, :red]}}

    Todo.Database.store(:test_alice, data)
    data_from_store = Todo.Database.get(:test_alice)

    assert data == data_from_store
  end

  test "saves state of the todo list" do
    todo = Todo.Item.new(~D[2021-01-01], "Hello from 2021")

    alice = Todo.Cache.server_process("test_alice")
    Todo.Server.add_entry(alice, todo)

    expected = [%Todo.Item{todo | id: 1}]

    assert expected == Todo.Server.entries(alice, ~D[2021-01-01])
    assert expected == Todo.Database.get("test_alice") |> Todo.List.entries(~D[2021-01-01])
  end

  defp remove_test_files do
    File.rm(".db/test_alice")
  end
end
