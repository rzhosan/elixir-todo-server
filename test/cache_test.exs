defmodule Todo.Cache.Test do
  use ExUnit.Case
  doctest Todo.Cache

  test "verify todo cash" do
    alice = Todo.Cache.server_process("alice")
    bob = Todo.Cache.server_process("bob")

    assert alice != bob
    assert alice == Todo.Cache.server_process("alice")
  end
end
