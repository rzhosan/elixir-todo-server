defmodule Todo.Server.Test do
  use ExUnit.Case
  doctest Todo.Server

  setup do
    remove_test_files()

    on_exit(fn -> remove_test_files() end)
  end

  test "verify server functions" do
    todo_server = Todo.Cache.server_process("test_list")

    Todo.Server.import_csv(todo_server, "./test/todo.csv")
    Todo.Server.delete_entry(todo_server, 2)
    Todo.Server.delete_entry(todo_server, 2)

    Todo.Server.add_entry(todo_server, Todo.Item.new(~D[2022-01-01], "Hello one more time"))
    Todo.Server.update_entry(todo_server, 3, fn item -> %Todo.Item{item | date: ~D[2021-01-01]} end)
    entries = Todo.Server.entries(todo_server, ~D[2021-01-01])

    assert [
      %Todo.Item{id: 1, date: ~D[2021-01-01], title: "hello"},
      %Todo.Item{id: 3, date: ~D[2021-01-01], title: "hello from the future"},
    ] = entries
  end

  defp remove_test_files do
    File.rm(".db/test_list")
  end
end
